---
title: "[Juniors CTF] Unbridled Joy Only!"
date: 2017-12-03T19:34:53+02:00
subtitle: "Writeup by ipu"
tags: ["Juniors CTF", "ipu", "joy"]
---

In that challenge we only got a 7z file called `en.7z` that contained 900 images.
We quickly noticed that the images connected to each other, for example images
186-188 had the same head in them so we needed to connect the images but how can
we know the size of a row?

There were many blocks of 4 gray images that were the border (the first row was
all gray too) so we can see that each row had 30 images, now we only need to
connect them.

The easiest way to connect the images that I found was using the Python library
PIL (called pillow on pip), this is the code I used:

```
#!/usr/bin/env python

from PIL import Image

# List of all images we need to connect
images = ['part ({})'.format(i) for i in range(1, 901)]

# The total dimension of the new image
new_im = Image.new('RGB', (43 * 30, 50 * 30))

x_offset = 0
y_offset = 0

for i, image_name in enumerate(images):
    # Every 30 images we need to move to the next row
    if i % 30 == 0:
        x_offset = 0
        y_offset += 49

    im = Image.open(image_name)
    new_im.paste(im, (x_offset, y_offset))
    x_offset += im.size[0]

new_im.save('out.jpg')
```

And here is the output of that script:

![You have to take a picture of your team with the cosplay of Rick and Morty. Also on the photo should be the name of the team and monitor with an open site of these competitions. Send photos directly to https://t.me/joinchat/GgpjyA_0kazxVML0vp3ieg or you can upload it somewhere and send a link to the chat. After verification, someone from the devteam will send you a flag in a personal. P.S. Only hardcode, creative and fun, and not a bad and sloppy photoshop! =)](out.jpg)

As you can see the image is not perfect but it was good enough to read what is
the next step. After that we join the telegram group and sent a picture of one
of the team member with a drawing of Pickle Rick.

