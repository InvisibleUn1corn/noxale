---
title: "[Juniors CTF] the Salvation of Liberty"
date: 2017-12-03T19:05:42+02:00
subtitle: "Writeup by ipu"
tags: ["Juniors CTF", "ipu", "forensic"]
---

### The task
Abradolf Lincler decided to destroy the most valuable monument on earth. To this
end, he has selected the top 10 monuments and photographed each to have a final
choice. Rick tried to stop him, but all he managed to get somewhat of [hard disks][1]
from his computer. Rick also learned that Lincler chosen for its sinister action
of the Statue of Liberty.  
Find out in what city Lincler preparing your act of vandalism to stop him. (Flag
- city name on russian or english) 

We got a 7zip file with 4 img files in it and we noticed that one of them is
missing. Using the `file` command we found out those img files were RAID5 files.

The easiest way to recover the content of the image files without the missing disk
was using [ReclaiMe Pro][2]. After recovering the files, we found 10 images. The
task said Abradol Lincler chose the Statue of Liberty and in the first image was
a picture of a Statue that looks like the Statue of Liberty.

![The image with the statue]()

From the file we extracted the location of the picture using an online tool that
can read EXIF data. The flag was `Nizhny Tagil`, the city where the picture was
taken.

[1]: disks.7z
[2]: https://www.reclaime.com/
