---
title: " [RC3-2017 CTF] Science Class"
date: 2017-12-01T18:12:59+02:00
subtitle: "Writeup by Ninjailbreak"
tags: ["rc3 2017", "ninjailbreak"]
aliases:
  - /2017/11/rc3-2017-ctf-science-class-writeup-by.html
---

**The Mission:**  
This is going to be a great year!  
Barely five minutes into chemistry and your friend passes you this note
inviting you to a party!  
What do they want you to bring?  


We easily can recognize the "[Periodic Table][1]" in the PDF file, so we combine
the table with the PDF and got a list of elements

![Periodic Table](Periodic Table.png)

Be, S, Ga, Se, Er.

after some combinations we guess the string "BeErGaSeS"  
changing the cases and submit the flag  
>> RC3-2017{BeerGases} <<

[1]: https://en.wikipedia.org/wiki/Periodic_table
