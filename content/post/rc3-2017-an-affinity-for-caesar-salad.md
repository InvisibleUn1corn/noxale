---
title: "[RC3-2017 CTF] An Affinity for Caesar Salad"
date: 2017-12-01T18:02:57+02:00
subtitle: "Writeup by Ninjailbreak"
tags: ["rc3 2017", "ninjailbreak", "crypto"]
aliases:
  - /2017/11/rc3-2017-ctf-affinity-for-caesar-salad.html
---

**The Mission:**  
Decrypt me! RC3-2017{GUPNCH_AITIL}

We understand that we need to use two common ciphers, Affine and Caesar.  
We brute forcing the string!

GUPNCH --- [affine](http://www.dcode.fr/affine-cipher) ---> GARDEN  
AITIL --- [caesar](http://www.dcode.fr/caesar-cipher) ---> SALAD  

Together we got a great salad :)  

**The Flag:**  
RC3-2017{GARDEN_SALAD}
